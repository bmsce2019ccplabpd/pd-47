#include<stdio.h>
void swap_callby_ref(int*,int*);
int main()
{
int c=3,d=4;
printf("in main c=%d & d=%d",c,d);
swap_callby_ref(&c,&d);
printf("in main c=%d and d=%d",c,d);
return 0;
}
   void swap_callby_ref(int *c,int *d)
   {
       int temp;
       temp=*c;
       *c=*d;
       *d=temp;
  printf("in fun(swap_callby_ref)c=%d and d=%d",*c,*d);
   
  }        
