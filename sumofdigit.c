#include<stdio.h>

 int SUM(int x)
{
 int s=0, i, rem, q;
  q=x;
  for(i=0; q!=0; ++i)
   {
     rem=q%10;
      s+=rem;
       q=q/10;
   }
 return s;
}

 int main()
{ 
 int a, sum;
  printf("\nEnter any number: ");
  scanf("%d", &a);
    sum=SUM(a);
  printf("\nThe sum of the digits is: %d\n", sum);
 return 0;
}